package rs.etf.km070233d.util;

public class MatrixFormater {

	public static String getFormatedMatrix(String matrix, int level) {
		String[] array = matrix.split("_");
		StringBuffer sb = new StringBuffer();
		sb.append("<html><table border=0 cellpadding=0 cellspacing=0px "
				+ "style=\"border-left:1px solid #000; border-right:1px solid #000; color:#000\"><tr>"
				+ "<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">"
				+ "&nbsp</td><td><table border=0 cellpadding=2 cellspacing=2 style=\"color:#000;\">");
		for (int i = 0; i < level; i++) {
			sb.append("<tr>");
			for (int j = 0; j < level; j++) {
				sb.append("<td align=\"center\" valign=\"center\" width=30>"
						+ array[level*i + j] + "</td>");
			}
			sb.append("</tr>");
		}

		sb.append("</tr></table></td>"
				+ "<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">&nbsp</td>"
				+ "</tr></table></html>");

		return sb.toString();
	}

}
