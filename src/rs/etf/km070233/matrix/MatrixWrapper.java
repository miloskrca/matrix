package rs.etf.km070233.matrix;

/**
 * User: Milos Krsmanovic Date: 8/20/12 Time: 9:45 PM
 */
public class MatrixWrapper {
	private int m;
	private int n;
	private String[][] matrix = null;

	/*
	 * Constructors
	 */

	public MatrixWrapper(int m, int n) {
		this.m = m;
		this.n = n;
		this.matrix = new String[m][n];
	}

	public MatrixWrapper() {
		this.m = 1;
		this.n = 1;
	}

	/*
	 * Getters and Setters
	 */

	public int getM() {
		if (this.m == this.n)
			return m;
		return -1;
	}

	public void setM(int m) {
		this.m = m;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public String get(int m, int n) {
		return matrix[m][n];
	}

	public MatrixWrapper set(int m, int n, String val) {
		matrix[m][n] = val;
		return this;
	}

	public String[][] getMatrix() {
		return this.matrix;
	}

	public int size() {
		return m;
	}

	public int[] getSmallest() {
		return null;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < this.m; i++) {
			sb.append("[");
			for (int j = 0; j < this.n; j++) {
				sb.append(( j==0 ? " " : ", ") + matrix[i][j]);
			}
			sb.append(" ]\n");
		}
		return sb.toString();
	}
}
