package rs.etf.km070233.matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rs.etf.km070233.gui.MainFrame;
import rs.etf.km070233.language.LanguageLoader;

public class MatrixFileReader {

	private MatrixWrapper matrix;
	private String matrixString;
	private String file;
	private int matrixSize;

	public MatrixFileReader(String file) throws Exception {
		super();
		this.file = file;
		this.matrixString = null;
		this.loadMatrixString();
	}

	private void loadMatrixString() throws Exception {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(
					this.file)));
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				sb.append(line + " ");
			}
			this.setMatrixString(sb.toString());
			reader.close();

			this.loadMatrix();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadMatrix() throws Exception {
		ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
		try {
			Pattern rowPattern = Pattern.compile("(.*?);");
			Matcher rowMacher = rowPattern.matcher(this.getMatrixString());

			int i = 0;
			while (rowMacher.find()) {
				list.add(new ArrayList<String>());
				String[] elemArray = rowMacher.group().split(",");
				for (int j = 0; j < elemArray.length; j++) {
					elemArray[j] = elemArray[j].replace(";", "").replace(" ",
							"");
					list.get(i).add(elemArray[j]);
				}
				i++;
			}

			for (i = 0; i < list.size(); i++) {
				if (list.size() != list.get(i).size())
					throw new Exception(MainFrame.getAssets().get(LanguageLoader.ERR_WRONG_PROPORION));
			}
			if (2 > list.size() || 4 < list.size())
				throw new Exception(MainFrame.getAssets().get(LanguageLoader.ERR_WRONG_SIZE) + list.size());

			this.setMatrix(new MatrixWrapper(list.size(), list.get(0).size()));

			for (i = 0; i < list.size(); i++) {
				for (int j = 0; j < list.get(i).size(); j++) {
					this.getMatrix().set(i, j, list.get(i).get(j));
				}
			}

			this.setMatrixSize(list.size());
		} catch (Exception e) {
			if (e.getMessage() != null)
				throw e;
			else
				throw new Exception(MainFrame.getAssets().get(LanguageLoader.ERR_WRONG_FORMAT));
		}
	}

	public MatrixWrapper getMatrix() {
		return matrix;
	}

	public void setMatrix(MatrixWrapper matrix) {
		this.matrix = matrix;
	}

	public String getMatrixString() {
		return matrixString;
	}

	public void setMatrixString(String matrixString) {
		this.matrixString = matrixString;
	}

	public int getMatrixSize() {
		return matrixSize;
	}

	public void setMatrixSize(int matrixSize) {
		this.matrixSize = matrixSize;
	}

}
