package rs.etf.km070233.language;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import rs.etf.km070233.gui.MainFrame;

public class LanguageLoader {

	private final Object LANG_LIST_MARKER = "###LANGUAGE###";
	private String file;
	private HashMap<String, HashMap<String, String>> list;

	public static String START_BTN = "startBtn";
	public static String STOP_BTN = "stopBtn";
	public static String EDIT_BTN = "editBtn";
	public static String CHOOSE_FORM_LABEL = "chooseFormLabel";
	public static String CHOOSE_FILE_BTN = "chooseFileBtn";
	public static String CHOOSE_LANGUAGE_BTN = "chooseLanguageLabel";
	public static String CLEAR_BTN = "clearBtn";
	public static String CALCULATING_LABEL = "calculatingLabel";
	public static String ERR_WRONG_SIZE = "errWrongSize";
	public static String ERR_WRONG_FORMAT = "errWrongFormat";
	public static String ERR_WRONG_PROPORION = "errWrongProportion";

	public LanguageLoader(String file) {
		this.file = file;
		this.list = new HashMap<String, HashMap<String, String>>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(
					this.file)));
			String line;

			String currentLanguage = null;

			// Read languages and assets
			while ((line = reader.readLine()) != null) {
				if (!line.equals("")) {
					if (line.equals(this.LANG_LIST_MARKER)) {
						currentLanguage = null;
					} else {
						if (currentLanguage == null) {
							currentLanguage = line;
							list.put(currentLanguage,
									new HashMap<String, String>());
						} else {
							String[] array = line.split("=");
							list.get(currentLanguage).put(array[0], array[1]);
						}
					}

				}

			}
			reader.close();

		} catch (Exception e) {
			MainFrame.writeRight(e.toString());
		}
	}

	public HashMap<String, String> getLanguage(String language) {
		return this.list.get(language);
	}

	public String[] getLanguages() {
		return this.list.keySet().toArray(new String[] {});
	}

}
