package rs.etf.km070233.python;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.python.core.PyInteger;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;

import rs.etf.km070233.matrix.MatrixWrapper;

public class ConcretePyMatrixHandler {

	private static final String FILE = "python_script.py";

	private static final String FUNC_SET = "SET";
	private static final String FUNC_GET_SMALLEST = "GET_SMALLEST";
	private static final String FUNC_DIV_POLYNOMMIALS = "DIV_POLYNOMMIALS";
	private static final String FUNC_MOVE_SMALLEST_TO_START = "MOVE_SMALLEST_TO_START";
	private static final String FUNC_MOVE_SMALLEST_TO_START_IN_COLUMN = "MOVE_SMALLEST_TO_START_IN_COLUMN";
	private static final String FUNC_GET = "GET";
	private static final String FUNC_PROCESS_ROWS = "PROCESS_ROWS";
	private static final String FUNC_PROCESS_COLUMNS = "PROCESS_COLUMNS";
	private static final String FUNC_PROCESS_COLUMNS_AFTER_HERMITIAN = "PROCESS_COLUMNS_AFTER_HERMITIAN";
	private static final String FUNC_IS_ROW_CLEARED = "IS_ROW_CLEARED";
	private static final String FUNC_IS_COLUMN_CLEARED = "IS_COLUMN_CLEARED";
	private static final String FUNC_IS_ROW_DIVIDABLE = "IS_ROW_DIVIDABLE";
	private static final String FUNC_IS_DIAGONAL_OK = "IS_DIAGONAL_OK";
	private static final String FUNC_ADD_ROWS = "ADD_ROWS";
	private static final String FUNC_NEEDS_FIXING = "NEEDS_FIXING";
	private static final String FUNC_SWITCH_COLUMNS = "SWITCH_COLUMNS";
	private static final String FUNC_FIX_FIRST_ELEMENT = "FIX_FIRST_ELEMENT";

	private MatrixWrapper matrixWrapper;
	private PythonInterpreter interp;

	public ConcretePyMatrixHandler(MatrixWrapper matrixWrapper) {
		super();
		this.matrixWrapper = matrixWrapper;
		this.interp = new PythonInterpreter();
		prepareMatrix();
	}

	public void prepareMatrix() {
		for (int i = 0; i < this.matrixWrapper.getM(); i++) {
			for (int j = 0; j < this.matrixWrapper.getN(); j++) {
				StringBuffer c = new StringBuffer();
				c.append((char) (97 + i)).append(j);
				this.interp.set(c.toString(),
						new PyString(this.matrixWrapper.getMatrix()[i][j]));
			}
		}
		this.interp.set("size", new PyString(this.matrixWrapper.getM() + "x"
				+ this.matrixWrapper.getN()));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_SET));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);
	}

	public String addColumns(int col1, int col2) {
		this.interp.set("col1", new PyInteger(col1));
		this.interp.set("col2", new PyInteger(col2));
		this.interp.set("operation", new PyInteger(2));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}

	public String divPolynomials(String poly1, String poly2) {
		this.interp.set("poly1", new PyString(poly1));
		this.interp.set("poly2", new PyString(poly2));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_DIV_POLYNOMMIALS));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return "division: q: " + this.interp.get("q").toString() + "   r: "
				+ this.interp.get("r").toString();
	}

	public String getSmallest(int lvl, int rng) {
		this.interp.set("lvl", new PyInteger(lvl));
		this.interp.set("rng", new PyInteger(rng));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_GET_SMALLEST));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return "smallest: i: " + this.interp.get("smallest_i").toString()
				+ " j: " + this.interp.get("smallest_j").toString();
	}

	public String moveSmallestToStart(int lvl, int rng) {
		this.interp.set("lvl", new PyInteger(lvl));
		this.interp.set("rng", new PyInteger(rng));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_MOVE_SMALLEST_TO_START));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}

	public String moveSmallestToStartInColumn(int lvl, int rng) {
		this.interp.set("lvl", new PyInteger(lvl));
		this.interp.set("rng", new PyInteger(rng));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_MOVE_SMALLEST_TO_START_IN_COLUMN));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}

	public String processRows(int row1, int row2, int column, int rng) {
		this.interp.set("row1", new PyInteger(row1));
		this.interp.set("row2", new PyInteger(row2));
		this.interp.set("column", new PyInteger(column));
		this.interp.set("rng", new PyInteger(rng));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_PROCESS_ROWS));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}

	public String processColumns(int column1, int column2, int row, int rng) {
		this.interp.set("column1", new PyInteger(column1));
		this.interp.set("column2", new PyInteger(column2));
		this.interp.set("row", new PyInteger(row));
		this.interp.set("rng", new PyInteger(rng));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_PROCESS_COLUMNS));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}

	public String get(int row, int column) {
		this.interp.set("row", new PyInteger(row));
		this.interp.set("column", new PyInteger(column));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_GET));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("value").toString();
	}

	public int getSize() {
		return matrixWrapper.getM();
	}

	public boolean isRowCleared(int subMatrixLvl) {
		this.interp.set("level", new PyInteger(subMatrixLvl));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_IS_ROW_CLEARED));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("cleared").toString().equals("True");
	}

	public boolean isColumnCleared(int subMatrixLvl) {
		this.interp.set("level", new PyInteger(subMatrixLvl));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_IS_COLUMN_CLEARED));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("cleared").toString().equals("True");
	}

	public boolean isRowDividable(int subMatrixLvl) {
		this.interp.set("level", new PyInteger(subMatrixLvl));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_IS_ROW_DIVIDABLE));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("dividable").toString().equals("True");
	}

	public String processColumnsAfterHermitian(int row1, int row2, int column) {
		this.interp.set("row1", new PyInteger(row1));
		this.interp.set("row2", new PyInteger(row2));
		this.interp.set("column", new PyInteger(column));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_PROCESS_COLUMNS_AFTER_HERMITIAN));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}


	public boolean isDiagonalOk() {
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_IS_DIAGONAL_OK));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("diagonal_ok").toString().equals("True");
	}
	
	public String addRows(int row1, int row2) {
		this.interp.set("row1", new PyInteger(row1));
		this.interp.set("row2", new PyInteger(row2));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_ADD_ROWS));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}
	
	public boolean needsFixing(int level) {
		this.interp.set("level", new PyInteger(level));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_NEEDS_FIXING));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("needs_fixing").toString().equals("True");
	}

	public String switchColumns(int column1, int column2) {
		this.interp.set("column1", new PyInteger(column1));
		this.interp.set("column2", new PyInteger(column2));
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_SWITCH_COLUMNS));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("matrix").toString();
	}

	public String fixFirstElement() {
		this.interp.set("operation", new PyString(
				ConcretePyMatrixHandler.FUNC_FIX_FIRST_ELEMENT));
		this.interp.execfile(ConcretePyMatrixHandler.FILE);

		return this.interp.get("leading_coefficient").toString();
	}
	
	public String toString() {
		String matrix = this.interp.get("matrix").toString();
		StringBuffer sb = new StringBuffer();

		Pattern pattern = Pattern.compile("(\\[).*(\\])");
		Matcher matcher = pattern.matcher(matrix);
		while (matcher.find()) {
			String poly = matcher.group();
			String value = poly.replace("[", "").replace("]", "")
					.replace("Poly(", "").replace(", x)", "").replace(" ", "");
			for (int i = 0; i < value.split(",").length; i++) {
				sb.append("_" + value.split(",")[i]);
			}
		}
		return sb.toString().substring(1);
	}


}
