package rs.etf.km070233.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import rs.etf.km070233.forms.HermitianForm;
import rs.etf.km070233.forms.MatrixForm;
import rs.etf.km070233.forms.SmithsForm;
import rs.etf.km070233.language.LanguageLoader;
import rs.etf.km070233.matrix.MatrixFileReader;
import rs.etf.km070233.python.ConcretePyMatrixHandler;

public class MainFrame {

	private static final int SMITHS = 0;
	private static final int HERMITIAN = 1;

	private static JFrame frmJavaAppletFor;
	private static LanguageLoader langLoader = new LanguageLoader(
			"languages.txt");
	private boolean work = true;
	private MatrixForm[] forms = new MatrixForm[2];
	private static JPanel loaderHolderPanel;
	final JLabel fileName = new JLabel("");
	final JComboBox<String> formChooser = new JComboBox<String>();
	private static HashMap<String, String> assets;
	private static JButton stopBtn = new JButton("Stop");
	static JButton startBtn = new JButton("Start");
	static JButton btnEditFile = new JButton("Edit file");
	static JButton btnChooseFile = new JButton("Choose file");
	static JLabel lblChooseLnguage = new JLabel("Choose language:");
	static JLabel lblChooseForm = new JLabel("Choose form: ");
	static JButton clearLeft = new JButton("Clear");
	static JButton clearRight = new JButton("Clear");
	static JLabel calculatinglbl = new JLabel("calculating...", new ImageIcon(
			"images/ajax-loader.gif"), JLabel.CENTER);
	static JPanel rightDisplayPanel = new JPanel();
	static JPanel leftDisplayPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new MainFrame();
					MainFrame.frmJavaAppletFor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmJavaAppletFor = new JFrame();
		frmJavaAppletFor
				.setTitle("Java applet for Smiths and Hermitian forms of matrices");
		frmJavaAppletFor.setPreferredSize(new Dimension(1024, 600));
		frmJavaAppletFor.setBounds(100, 100, 1024, 600);
		frmJavaAppletFor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmJavaAppletFor.getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel topPanel = new JPanel();
		topPanel.setPreferredSize(new Dimension(10, 75));
		topPanel.setBorder(null);
		topPanel.setMinimumSize(new Dimension(10, 75));
		topPanel.setBackground(SystemColor.scrollbar);
		frmJavaAppletFor.getContentPane().add(topPanel, BorderLayout.NORTH);
		topPanel.setLayout(new BorderLayout(0, 0));

		JPanel topTop = new JPanel();
		topTop.setBackground(Color.DARK_GRAY);
		FlowLayout fl_topTop = (FlowLayout) topTop.getLayout();
		fl_topTop.setAlignment(FlowLayout.LEFT);
		topPanel.add(topTop, BorderLayout.NORTH);

		btnChooseFile.setBackground(Color.WHITE);
		topTop.add(btnChooseFile);
		btnChooseFile.setSize(new Dimension(40, 20));

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null,
				null, null));
		topTop.add(panel_2);
		panel_2.add(fileName);
		fileName.setBackground(Color.WHITE);
		fileName.setBorder(null);
		fileName.setPreferredSize(new Dimension(300, 14));
		fileName.setSize(new Dimension(300, 14));

		btnEditFile.setBackground(Color.WHITE);
		topTop.add(btnEditFile);
		btnEditFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = fileName.getText();
				if (!name.equals(""))
					try {
						Desktop.getDesktop().edit(new File(name));
					} catch (IOException e) {
						writeLeft(e.toString());
					}
			}
		});

		lblChooseForm.setForeground(Color.LIGHT_GRAY);
		topTop.add(lblChooseForm);
		formChooser.setBackground(Color.WHITE);
		topTop.add(formChooser);
		formChooser.setPreferredSize(new Dimension(150, 20));
		formChooser.setModel(new DefaultComboBoxModel<String>(new String[] {
				"Smith's", "Hermitian" }));

		JPanel topBottom = new JPanel();
		topBottom.setBackground(Color.DARK_GRAY);
		topPanel.add(topBottom, BorderLayout.CENTER);
		topBottom.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		startBtn.setBackground(Color.WHITE);
		topBottom.add(startBtn);
		startBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String name = fileName.getText();

				if (name.equals(""))
					return;

				setWork(true);
				startLoader();

				try {
					Thread t = new Thread(new Runnable() {

						private MatrixFileReader fileReader;
						private ConcretePyMatrixHandler handler;

						public void run() {

							String name = fileName.getText();

							try {
								fileReader = new MatrixFileReader(name);
								handler = new ConcretePyMatrixHandler(
										fileReader.getMatrix());
							} catch (Exception e) {
								String message = "";
								if (e.getMessage() != null) {
									message = e.getMessage();
								} else {
									message = e.toString();
								}
								MainFrame.stopLoader();
								JOptionPane.showMessageDialog(frmJavaAppletFor,
										message);
								MainFrame.writeRight(message);
								return;
							}

							if (work) {

								switch (formChooser.getSelectedIndex()) {
								case 0:
									forms[MainFrame.SMITHS] = new SmithsForm(
											handler);
									forms[MainFrame.SMITHS].calc();
									break;

								case 1:
									forms[MainFrame.HERMITIAN] = new HermitianForm(
											handler);
									forms[MainFrame.HERMITIAN].calc();
									break;

								default:
									break;
								}
							}

							setWork(true);
							stopLoader();

						}
					});
					t.start();

				} catch (Exception e) {
					writeRight(e.toString());
					System.out.print("\n" + e.toString() + "\n");
				}
			}
		});

		stopBtn.setBackground(Color.WHITE);
		stopBtn.setEnabled(false);
		topBottom.add(stopBtn);

		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		topBottom.add(panel);

		panel.add(lblChooseLnguage);
		lblChooseLnguage.setForeground(Color.LIGHT_GRAY);

		final JComboBox<String> languageChooser = new JComboBox<String>();
		languageChooser.setModel(new DefaultComboBoxModel<String>(langLoader
				.getLanguages()));
		languageChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setLanguage((String) languageChooser.getSelectedObjects()[0]);
			}
		});
		panel.add(languageChooser);
		languageChooser.setPreferredSize(new Dimension(150, 20));
		languageChooser.setBackground(Color.WHITE);
		stopBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setWork(false);
				stopLoader();
			}
		});
		btnChooseFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new java.io.File("."));
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {

					try {
						fileName.setText(fileChooser.getSelectedFile()
								.getName());
						leftDisplayPanel.removeAll();
						leftDisplayPanel.repaint();
						frmJavaAppletFor.pack();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		});

		JPanel centerPanel = new JPanel();
		centerPanel.setPreferredSize(new Dimension(800, 10));
		centerPanel.setSize(new Dimension(800, 10));
		frmJavaAppletFor.getContentPane().add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.X_AXIS));

		JPanel centerLeft = new JPanel();
		centerPanel.add(centerLeft);
		centerLeft.setBorder(new LineBorder(new Color(0, 0, 0)));
		centerLeft.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		centerLeft.add(scrollPane);

		scrollPane.setViewportView(leftDisplayPanel);
		leftDisplayPanel.setLayout(new BoxLayout(leftDisplayPanel,
				BoxLayout.Y_AXIS));

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		centerLeft.add(panel_1, BorderLayout.WEST);

		JPanel centerRight = new JPanel();
		centerPanel.add(centerRight);
		centerRight.setBorder(new LineBorder(new Color(0, 0, 0)));
		centerRight.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPaneRight = new JScrollPane();
		centerRight.add(scrollPaneRight);

		scrollPaneRight.setViewportView(rightDisplayPanel);
		rightDisplayPanel.setLayout(new BoxLayout(rightDisplayPanel,
				BoxLayout.Y_AXIS));
		
		JPanel panel_3 = new JPanel();
		centerRight.add(panel_3, BorderLayout.WEST);

		JPanel southPanel = new JPanel();
		southPanel.setBackground(Color.WHITE);
		frmJavaAppletFor.getContentPane().add(southPanel, BorderLayout.SOUTH);

		clearLeft.setForeground(Color.WHITE);
		clearLeft.setBackground(Color.DARK_GRAY);
		southPanel.add(clearLeft);

		loaderHolderPanel = new JPanel();
		southPanel.add(loaderHolderPanel);
		loaderHolderPanel.setBackground(Color.WHITE);
		FlowLayout fl_loaderHolderPanel = (FlowLayout) loaderHolderPanel
				.getLayout();
		fl_loaderHolderPanel.setAlignment(FlowLayout.RIGHT);

		JLabel lblNewLabel = new JLabel("                              ");
		loaderHolderPanel.add(lblNewLabel);

		clearRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				rightDisplayPanel.removeAll();
				rightDisplayPanel.repaint();
				frmJavaAppletFor.pack();
				setWork(true);
				stopLoader();
			}
		});
		clearRight.setHorizontalAlignment(SwingConstants.RIGHT);
		clearRight.setForeground(Color.WHITE);
		clearRight.setBackground(Color.DARK_GRAY);
		southPanel.add(clearRight);
		clearLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				leftDisplayPanel.removeAll();
				leftDisplayPanel.repaint();
				frmJavaAppletFor.pack();
				setWork(true);
				stopLoader();
			}
		});

		setLanguage((String) languageChooser.getSelectedObjects()[0]);

	}

	private void setWork(boolean work) {
		this.work = work;
		for (int i = 0; i < forms.length; i++)
			if (forms[i] != null)
				forms[i].setWork(work);
	}

	public static void startLoader() {
		loaderHolderPanel.removeAll();
		loaderHolderPanel.add(calculatinglbl);
		loaderHolderPanel.repaint();
		frmJavaAppletFor.pack();

		btnChooseFile.setEnabled(false);
		btnEditFile.setEnabled(false);
		startBtn.setEnabled(false);
		stopBtn.setEnabled(true);
	}

	public static void stopLoader() {
		loaderHolderPanel.removeAll();
		loaderHolderPanel.repaint();

		btnChooseFile.setEnabled(true);
		btnEditFile.setEnabled(true);
		startBtn.setEnabled(true);
		stopBtn.setEnabled(false);
	}

	public static void writeLeft(String string) {
		leftDisplayPanel.add(new JLabel(string/*, BoxLayout.X_AXIS*/));
		leftDisplayPanel.repaint();
		frmJavaAppletFor.pack();
	}

	public static void writeRight(String string) {
		rightDisplayPanel.add(new JLabel(string/*, BoxLayout.X_AXIS*/));
		rightDisplayPanel.repaint();
		frmJavaAppletFor.pack();
	}

	private void setLanguage(String language) {
		assets = langLoader.getLanguage(language);
		MainFrame.startBtn.setText(assets.get(LanguageLoader.START_BTN));
		MainFrame.stopBtn.setText(assets.get(LanguageLoader.STOP_BTN));
		MainFrame.btnEditFile.setText(assets.get(LanguageLoader.EDIT_BTN));
		MainFrame.btnChooseFile.setText(assets
				.get(LanguageLoader.CHOOSE_FILE_BTN));
		MainFrame.lblChooseLnguage.setText(assets
				.get(LanguageLoader.CHOOSE_LANGUAGE_BTN));
		MainFrame.lblChooseForm.setText(assets
				.get(LanguageLoader.CHOOSE_FORM_LABEL));
		MainFrame.clearLeft.setText(assets.get(LanguageLoader.CLEAR_BTN));
		MainFrame.clearRight.setText(assets.get(LanguageLoader.CLEAR_BTN));
		MainFrame.calculatinglbl.setText(assets
				.get(LanguageLoader.CALCULATING_LABEL));
	}
	
	public static HashMap<String,String> getAssets() {
		return MainFrame.assets;
	}

}
