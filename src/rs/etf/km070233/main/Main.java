package rs.etf.km070233.main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Krca Date: 8/20/12 Time: 9:24 PM
 */
public class Main {
	public static void main(String args[]) {
		Pattern pattern = Pattern.compile("(\\[).*(\\])");
		Matcher matcher = pattern
				.matcher("[Poly(x**2+6, x), Poly(6, x)]\n[Poly(0, x), Poly(1, x)]");
		while (matcher.find()) {
			String poly = matcher.group();
			String value = poly.replace("[", "").replace("]", "")
					.replace("Poly(", "").replace(", x)", "").replace(" ", "");
			for (int i = 0; i < value.split(",").length; i++)
				System.out.println(value.split(",")[i]);
		}
	}
}
