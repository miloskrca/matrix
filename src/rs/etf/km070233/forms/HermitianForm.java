package rs.etf.km070233.forms;

import rs.etf.km070233.gui.MainFrame;
import rs.etf.km070233.python.ConcretePyMatrixHandler;
import rs.etf.km070233d.util.MatrixFormater;

public class HermitianForm extends MatrixForm {

	private ConcretePyMatrixHandler handler;

	public HermitianForm(ConcretePyMatrixHandler handler) {
		this.handler = handler;
	}

	@Override
	public void calc() {

		MainFrame.writeLeft("===================================");
		MainFrame.writeRight("===================================");
		MainFrame.writeLeft("<html><span style='text-decoration:underline;'>Hermitian form</span></html>");
		MainFrame.writeRight("<html><span style='text-decoration:underline;'>Hermitian form</span></html>");
		MainFrame.writeLeft("===================================");
		MainFrame.writeRight("===================================");
		
		MainFrame.writeLeft("Starting matrix:");
		MainFrame.writeLeft("=============================");
		MainFrame.writeRight("Starting matrix:");
		MainFrame.writeRight("=============================");
		
		MainFrame.writeLeft(MatrixFormater.getFormatedMatrix(handler.toString(), handler.getSize()));
		MainFrame.writeRight(MatrixFormater.getFormatedMatrix(handler.toString(), handler.getSize()));

		for (int subMatrixLvl = 0; subMatrixLvl < handler.getSize() - 1; subMatrixLvl++) {

			MainFrame.writeLeft("Proccessing row " + subMatrixLvl
					+ " on level " + subMatrixLvl);
			MainFrame.writeLeft("=============================");
			do {

				handler.moveSmallestToStartInColumn(subMatrixLvl, handler.getSize());
				MainFrame.writeLeft("Moving smallest to start...");
				MainFrame.writeLeft("=============================");
				MainFrame.writeLeft(MatrixFormater.getFormatedMatrix(handler.toString(), handler.getSize()));
				
				for (int row2 = subMatrixLvl + 1; row2 < handler.getSize(); row2++) {
					handler.processRows(subMatrixLvl, row2,
							subMatrixLvl, handler.getSize());
					MainFrame.writeLeft(MatrixFormater.getFormatedMatrix(handler.toString(), handler.getSize()));
				}

				MainFrame.writeLeft("=============================");

			} while (!handler.isColumnCleared(subMatrixLvl) && work);

		}
		
		if (!work)
			return;

		/*
		 * If an element on the diagonal is of greater degree than the one
		 * "above" him in the matrix
		 */
		MainFrame.writeLeft("Checking elements on the diagonal...");
		MainFrame.writeLeft("============================");
		
		for (int subMatrixLvl = 1; subMatrixLvl < handler.getSize(); subMatrixLvl++) {
			for (int row2 = subMatrixLvl - 1; row2 >= 0; row2--) {
				handler.processColumnsAfterHermitian(subMatrixLvl, row2,
						subMatrixLvl);
				MainFrame.writeLeft(MatrixFormater.getFormatedMatrix(handler.toString(), handler.getSize()));
			}
		}
		
		MainFrame.writeLeft("Finished:");
		MainFrame.writeLeft("===================================");
		MainFrame.writeRight("Finished:");
		MainFrame.writeRight("===================================");
		MainFrame.writeLeft(MatrixFormater.getFormatedMatrix(handler.toString(), handler.getSize()));
		MainFrame.writeRight(MatrixFormater.getFormatedMatrix(handler.toString(), handler.getSize()));
		MainFrame.writeLeft("");
		MainFrame.writeRight("");

	}

}
