package rs.etf.km070233.forms;

public abstract class MatrixForm {
	protected boolean work = true;

	public abstract void calc();

	public void setWork(boolean work) {
		this.work = work;
	}
}
