from sympy import *
from sympy import Poly

###############################################
############# Symbols #########################
###############################################

x = Symbol('x')

###############################################
############# Functions #######################
###############################################

# Gets the smallest element from matrix
def get_smallest(level, rng):
	smallest_i = smallest_j = level
	for i in range(level, rng):
		for j in range(level, rng):
			degree_ij = (matrix[i, j]).degree
			degree_smallest = (matrix[smallest_i, smallest_j]).degree
			if((degree_ij <= degree_smallest) == True and matrix[i, j] != Poly(0,x)):
				if((degree_ij == degree_smallest) == True):
					if(matrix[i, j] < matrix[smallest_i, smallest_j]):
						smallest_i = i
						smallest_j = j
				else:
					smallest_i = i
					smallest_j = j
					
	return smallest_i, smallest_j
	
# Gets the smallest element but only in the column level
def get_smallest_in_column(level, rng):
	smallest_i = smallest_j = level
	j = level
	for i in range(level, rng):
		degree_ij = (matrix[i, j]).degree
		degree_smallest = (matrix[smallest_i, smallest_j]).degree
		if((degree_ij <= degree_smallest) == True and matrix[i, j] != Poly(0,x)):
			if((degree_ij == degree_smallest) == True):
				if(matrix[i, j] < matrix[smallest_i, smallest_j]):
					smallest_i = i
					smallest_j = j
			else:
				smallest_i = i
				smallest_j = j
					
	return smallest_i, smallest_j
	
# Moves the element with the smallest degree to position [level, level]
def move_smallest_to_start(level, columnsAllowed, rng):
	if(columnsAllowed):
		smallest_i, smallest_j = get_smallest(level, rng);
	else:
		smallest_i, smallest_j = get_smallest_in_column(level, rng);
	if(smallest_i > level):
		temp = matrix[smallest_i, :]
		matrix[smallest_i, :] = matrix[level, :]
		matrix[level, :] = temp
	if(smallest_j > level and columnsAllowed):
		temp = matrix[:, smallest_j]
		matrix[:, smallest_j] = matrix[:, level]
		matrix[:, level] = temp
	return
	
###############################################
############## Switch #########################
###############################################

# Sets up the matrix
if(operation=="SET"):
	if(size=="2x2"):
		matrix_size = 2
		matrix = Matrix([[Poly(a0, x), Poly(a1, x)], \
						[Poly(b0, x), Poly(b1, x)]])

	elif(size=="3x3"):
		matrix_size = 3
		matrix = Matrix([[Poly(a0, x), Poly(a1, x), Poly(a2, x)], \
						[Poly(b0, x), Poly(b1, x), Poly(b2, x)], \
						[Poly(c0, x), Poly(c1, x), Poly(c2, x)]])

	elif(size=="4x4"):
		matrix_size = 4
		matrix = Matrix([[Poly(a0, x), Poly(a1, x), Poly(a2, x), Poly(a3, x)], \
						[Poly(b0, x), Poly(b1, x), Poly(b2, x), Poly(b3, x)], \
						[Poly(c0, x), Poly(c1, x), Poly(c2, x), Poly(c3, x)], \
						[Poly(d0, x), Poly(d1, x), Poly(d2, x), Poly(d3, x)]])
	else:
		matrix = zeros(0, 0)

# Devides two polynomials, used for testing purpuses		
elif(operation=="DIV_POLYNOMMIALS"):
	q, r = pdiv(Poly(poly1, x), Poly(poly2, x))

# Calls get_smallest function and returns the result
elif(operation=="GET_SMALLEST"):
	smallest_i, smallest_j = get_smallest(lvl, rng);

# Calls move_smallest_to_start function and returns the result
elif(operation=="MOVE_SMALLEST_TO_START"):
	move_smallest_to_start(lvl, True, rng);

# Calls move_smallest_to_start function that  only uses column lvl and returns the result	
elif(operation=="MOVE_SMALLEST_TO_START_IN_COLUMN"):
	move_smallest_to_start(lvl, False, rng);

# Checks if the row level is cleared	
elif(operation=="IS_COLUMN_CLEARED"):
	cleared = True
	for i in range(level + 1, matrix_size):
		if(matrix[i, level] != Poly(0, x)):
			cleared = False

# Checks if the column level is cleared				
elif(operation=="IS_ROW_CLEARED"):
	cleared = True
	for i in range(level + 1, matrix_size):
		if(matrix[level, i] != Poly(0, x)):
			cleared = False
			
elif(operation=="IS_ROW_DIVIDABLE"):
	dividable = True
	for i in range(level + 1, matrix_size):
		if(matrix[level, i] != Poly(0, x)):
			if(matrix[level, level].degree > matrix[level, i].degree):
				dividable = False

# Processes the rows in a way necessary for Smith's and Hermitian form			
elif(operation=="PROCESS_ROWS"):
	if(matrix[row2, column] != Poly(0, x)):
		try:
			quo = Poly(quo(matrix[row2, column], matrix[row1, column]), x)
		except: # matrix[row2, column] = Poly(0, x) and the quo() function is stupid and can't deal with that
			quo = Poly(pquo(matrix[row2, column], matrix[row1, column]), x)
		neg_quo = quo * Poly(-1, x)
		for i in range(column, rng):
			partial = neg_quo * matrix[row1, i]
			matrix[row2, i] = partial + matrix[row2, i]
		
# Processes the columns in a way necessary for Smith's form
elif(operation=="PROCESS_COLUMNS"):
	if(matrix[row, column2] != Poly(0, x)):
		try:
			quo = Poly(quo(matrix[row, column2], matrix[row, column1]), x)
		except: # matrix[row, column2] = Poly(0, x) and the quo() function is stupid and can't deal with that
			quo = Poly(pquo(matrix[row, column2], matrix[row, column1]), x)
		neg_quo = quo * Poly(-1, x)
		for i in range(row, rng):
			partial = neg_quo * matrix[i, column1]
			matrix[i, column2] = partial + matrix[i, column2]

# Processes the columns in a way necessary for Hermitian form
elif(operation=="PROCESS_COLUMNS_AFTER_HERMITIAN"):
	if(matrix[row1, column] != Poly(0, x) and matrix[row2, column].degree >= matrix[row1, column].degree):
		try:
			quo = Poly(quo(matrix[row2, column], matrix[row1, column]), x)
		except: # matrix[row2, column] = Poly(0, x) and the quo() function is stupid and can't deal with that
			quo = Poly(pquo(matrix[row2, column], matrix[row1, column]), x)
		neg_quo = quo * Poly(-1, x)
		for i in range(column, matrix_size):
			partial = neg_quo * matrix[row1, i]
			matrix[row2, i] = partial + matrix[row2, i]

#checks the diagonal to see if all elements are aligned so a11 | a22 | a33 ...
elif(operation=="IS_DIAGONAL_OK"):
	diagonal_ok = True
	last_elem_degree = matrix[0, 0].degree
	for i in range(1, matrix_size):
		if(matrix[i, i] != Poly(0, x)):
			if(matrix[i, i].degree < last_elem_degree):
				diagonal_ok = False
		last_elem = matrix[i, i].degree

elif(operation=="ADD_ROWS"):
	matrix[row1,:] = matrix[row1,:] + matrix[row2,:]
	
elif(operation=="NEEDS_FIXING"):
	needs_fixing = False
	if(matrix[level, level] != Poly(0, x) and matrix[level+1, level+1] != Poly(0, x) and matrix[level, level].degree > matrix[level+1, level+1].degree):
		needs_fixing = True
		
elif(operation=="SWITCH_COLUMNS"):
	temp = matrix[:, column1]
	matrix[:, column1] = matrix[:, column2]
	matrix[:, column2] = temp
	
# trnsforms first element so it is equal to Poly(0, x)
elif(operation=="FIX_FIRST_ELEMENT"):
	leading_coefficient = matrix[0, 0].coeff(matrix[0, 0].degree)
	if(matrix[0, 0] != Poly(0, x) and leading_coefficient != 1):
		for i in range(0, matrix_size):
			matrix[0, i] = matrix[0, i] * (1 / leading_coefficient)

# Returs the value from the matrix in position [row, column]
elif(operation=="GET"):
	value = matrix[row, column]